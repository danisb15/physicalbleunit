/** @file uart.h
 *  @author Daniel Skomedal Breland
 *  @date 11 FEB
 *  @brief Driver for uart
 *
 * This .c and .h files are for the uart, sending data via BLE. With buttonpress.
 * The uart is inspired from Nordics SDK15.2 and a previous project we have done at school, author then was Daniel Ved�.
 *
 */

#ifndef INCLUSION_GUARD_UART_H
#define INCLUSION_GUARD_UART_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define UART_MAIL_SIZE 4
#define UART_BUFFER_SIZE 50


typedef struct{

  char buffer[UART_BUFFER_SIZE];


}uart_message_t;


/**
 * @brief Function for initializing the Uart Print module.
 *
 * This function configures the UART and creates the mutex to be used when calling UartPrintChar().
 *
 * @retval true  If initialization was successful.
 * @retval false If initialization failed.
 */
ret_code_t UartPrintInit(void);


void UartPrintf(const char* fmt, ...);
/**
 * @brief Function for sending one character to the UART.
 *
 * This function transmits one character via the UART. The function calls the UART in blocking mode
 * and the calling side must therefor gain exclusive access using the mutex declared in this file.
 *
 * @param[in] ch The character to be sent to the UART.
 *
 * @retval true  If print was successful.
 * @retval false If print failed.
 */
ret_code_t UartPrintChar(uint8_t ch);
 
 void UartPrint(void const *argument);
 
/**
 * @brief Function for sending char array to the UART.
 *
 * This function transmits a char array character via the UART.
 *
 * @param[in] formated char array
 *
 */
void UartPrintMessage(char *buffer);

#endif // INCLUSION_GUARD_UART_H
