/** @file uart.h
 *  @author Daniel Skomedal Breland
 *  @date 11 FEB
 *  @brief Driver for uart
 *
 * This .c and .h files are for the uart, sending data via BLE. With buttonpress.

 * The uart is inspired from Nordics SDK15.2 and a previous project we have done at school, author then was Daniel Ved�.
 *
 */
 

#include "nrf_drv_uart.h"
#include "sdk_common.h"
#include "pca10040.h"
#include "uart.h"
#include <stdarg.h>


static nrf_drv_uart_t uart_inst = NRF_DRV_UART_INSTANCE(0);

ret_code_t UartPrintInit(void)
{
	ret_code_t ret_code;
	
	nrf_drv_uart_config_t uart_config = NRF_DRV_UART_DEFAULT_CONFIG;
	
	uart_config.pseltxd = TX_PIN_NUMBER;
	uart_config.pselrxd = RX_PIN_NUMBER;
	uart_config.pselcts = CTS_PIN_NUMBER;
	uart_config.pselrts = RTS_PIN_NUMBER;
													 										 
	ret_code = nrf_drv_uart_init(&uart_inst, &uart_config, NULL);
	
	if (ret_code != NRF_SUCCESS) {
		return NRF_ERROR_INTERNAL;
	}
	return NRF_SUCCESS;
}

ret_code_t UartPrintChar(uint8_t ch)
{
	ret_code_t ret_code;
	ret_code = nrf_drv_uart_tx(&uart_inst, (const uint8_t *)&ch, 1);
	
	if (ret_code == NRF_SUCCESS) {
		return NRF_ERROR_INTERNAL;
	}
	return NRF_SUCCESS;
}

void UartPrintf(const char * fmt, ...){
	
	//Allocates a mailbox, returns a pointer to the free mailbox
	//uart_message_t *mail = (uart_message_t*) osMailAlloc(uart_mailq_id, 0);
	//Test if obtained mailbox pointer is a NULL pointer in case of error
	
	uart_message_t message;
	
	//Variable length argument list
	va_list argument_list;
	
	//Starts the variable argument list
	va_start(argument_list, fmt);
	
	//Sends formatted output to mail.buffer from argument list
	vsnprintf((char*)message.buffer, UART_BUFFER_SIZE, fmt, argument_list);
	
	//End the variable argument list
	va_end(argument_list);
	
	
	UartPrintMessage(message.buffer);
	//Put the mail in the queue
	//osMailPut(uart_mailq_id, mail);
	
	return;
}

//Print a formatted array of characters
void UartPrintMessage(char *buffer)
{
	char message[30];
	strcpy(message, buffer);
	
	for(uint32_t i = 0; i < strlen((char*)message); i++){/*Print message*/
		UartPrintChar(message[i]);
	}
	return;
}

// End of uart_print.c